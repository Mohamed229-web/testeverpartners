Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  
  # Defines the root path route ("/")
  root to: 'pages#home'
  
  devise_for :users, path: '', path_names:{sign_in:'login', sign_out:'logout', sign_up:'', registration:'register', confirmation:'confirmation'}, controllers: {confirmations: 'users/confirmations'}
              
                      
  devise_scope :user do
    get '/profil', to: 'devise/registrations#edit', as: :profil
  end

  # Operations
  resources :operations

end