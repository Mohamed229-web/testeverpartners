class CreateOperations < ActiveRecord::Migration[7.0]
  def change
    create_table :operations do |t|
      t.integer :operation_type
      t.integer :amount
      t.text :motif
      t.datetime :datetime

      t.timestamps
    end

    add_reference :operations, :user, foreign_key: true
  end
end
