FactoryBot.define do
  factory :operation do
    amount {50000}
    motif {"Achat de basket"}
    operation_type {0}
    datetime {DateTime.current.to_date}
    factory :operation1 do
      amount {""}
      motif {""}
    end
    factory :operation2 do
      amount {"skld50000f"}
      operation_type {""}
    end
    factory :operation3 do
      user {build(:user)}
    end
  end
end
