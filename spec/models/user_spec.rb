require 'rails_helper'


RSpec.describe User, type: :model do

  context 'validation' do
    it 'must have an email' do
      user = build(:user2)
      user.validate
      expect(user.errors.messages).to include(:email)
      expect(user.valid?).to be false
    end

    it 'must have a password' do
      user = build(:user2)
      user.validate
      expect(user.errors.messages).to include(:password)
      expect(user.valid?).to be false
    end

    it 'must have valid email' do
      user = build(:user)
      user.validate
      expect(user.email).to match(/\A([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})\z/)
    end
  end
end