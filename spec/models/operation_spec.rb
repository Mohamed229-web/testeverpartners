require 'rails_helper'


RSpec.describe Operation, type: :model do

  context "validation" do
    it 'must have an amount' do
      operation = build(:operation1)
      operation.validate
      expect(operation.errors.messages).to include(:amount)
      expect(operation.valid?).to be false
    end

    it 'must have a motif' do
      operation = build(:operation1)
      operation.validate
      expect(operation.errors.messages).to include(:motif)
      expect(operation.valid?).to be false
    end

    it 'must have a user associate' do
      operation = build(:operation1)
      operation.validate
      expect(operation.errors.messages).to include(:user)
      expect(operation.valid?).to be false
    end

    it 'must have a valid amount' do
      operation = build(:operation)
      operation.amount.to_i
      expect(operation.amount.is_a?Integer).to be true
      expect(operation.amount>0).to be true
    end
  end

end