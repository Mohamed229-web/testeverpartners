class ApplicationController < ActionController::Base

    add_flash_types :success, :danger
    before_action :configure_devise_parameters, if: :devise_controller?
    before_action :authenticate_user!

    private

    def only_signed_out
        redirect_to root_path, alert: "Vous êtes déjà connecté"  if user_signed_in?
    end

    protected

    def configure_devise_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :email, :password, :password_confirmation])
    end

    def after_sign_out_path_for(scope)
        new_user_session_path
    end
end
