class PagesController < ApplicationController

  def home
    @operations = Operation.where("user_id" => current_user.id).order(datetime: :desc).limit(5)
    all_expenses_op = Operation.where("user_id" => current_user.id, "operation_type" => 0)
    all_incomes_op = Operation.where("user_id" => current_user.id, "operation_type" => 1)
    @total_expenses = 0
    @total_incomes = 0
    
    all_expenses_op.each do |expense|
      @total_expenses += expense.amount
    end
    all_incomes_op.each do |income|
      @total_incomes += income.amount
    end
    
    @rest = @total_incomes - @total_expenses
  end
    
end