class OperationsController < ApplicationController

  before_action :set_operation, only: [:edit, :update, :show, :destroy]
  def index
    @operations = Operation.where("user_id" => current_user.id).order(datetime: :desc)
  end

  def new
    @operation = Operation.new
  end

  def create
    @operation = Operation.new(operation_params)
    @operation.user= current_user
    if @operation.valid?
      @operation.save
      redirect_to root_path, success: 'Opération ajoutée avec succès.'
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @operation.update(operation_params)
      redirect_to operations_path, success: "Opération modifiée avec succès"
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
      if @operation.destroy
        redirect_to operations_path, success: "Opération supprimée avec succès"
      end
  end

  private

  def operation_params
    params.require(:operation).permit(:amount, :motif, :operation_type, :datetime)
  end
	
  def set_operation
    @operation = Operation.find(params["id"])
  end
end