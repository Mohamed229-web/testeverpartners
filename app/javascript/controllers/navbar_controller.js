import { Controller } from "@hotwired/stimulus"

export default class extends Controller {

    static targets = [ "stateClose", "show", "stateOpen" ]

    mobile() {
        // console.log(this.state_closeTarget.classList.contains('hidden'))
        // alert("All is right!")
        this.stateCloseTarget.classList.toggle('hidden')
        this.stateOpenTarget.classList.toggle('hidden')
        this.showTarget.classList.toggle('hidden')
    }

    // connect(){
    //     alert("All is right! In Navbar")
    // }
}