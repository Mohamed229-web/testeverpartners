class Operation < ApplicationRecord

    belongs_to :user

    validates :amount, :motif, :operation_type, :datetime, presence: true
    validates :amount, numericality: true
    validates :user, presence: true
    
end
