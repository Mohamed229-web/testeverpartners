class User < ApplicationRecord

  attr_accessor :login

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  validates :username, presence: true, uniqueness: {case_sensitive: false}, format: {with: /\A[a-zA-Z0-9 _\.]*\z/}

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where("lower(username) = :value OR lower(email) = :value", value: login.downcase).first
    else
      where(conditions.to_hash)
    end
  end


  def self.my_confirm_by_token(confirmation_token)
    # When the `confirmation_token` parameter is blank, if there are any users with a blank
    # `confirmation_token` in the database, the first one would be confirmed here.
    # The error is being manually added here to ensure no users are confirmed by mistake.
    # This was done in the model for convenience, since validation errors are automatically
    # displayed in the view.
    if confirmation_token.blank?
      confirmable = new
      confirmable.errors.add(:confirmation_token, :blank)
      return confirmable
    end

    confirmable = where(confirmation_token: confirmation_token).first

    unless confirmable
      confirmation_digest = Devise.token_generator.digest(self, :confirmation_token, confirmation_token)
      confirmable = find_or_initialize_with_error_by(:confirmation_token, confirmation_digest)
    end

    # TODO: replace above lines with
    # confirmable = find_or_initialize_with_error_by(:confirmation_token, confirmation_token)
    # after enough time has passed that Devise clients do not use digested tokens

    confirmable.confirm if confirmable.persisted?
    confirmable
  end

  # def self.my_send_confirmation_instructions(attributes = {})
  #   confirmable = find_by_unconfirmed_email_with_errors(attributes) if reconfirmable
  #   unless confirmable.try(:persisted?)
  #     confirmable = find_or_initialize_with_errors(confirmation_keys, attributes, :not_found)
  #   end
  #   confirmable.resend_confirmation_instructions if confirmable.persisted?
  #   confirmable
  # end
end
